<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require 'vendor/autoload.php';

if (count($_POST) < 1)
{
    header('Location: http://condominioloscondores.com');
    exit();
}

$nombre = $_POST['form']['nombres'];
$telefono = $_POST['form']['telefono'];
$email = $_POST['form']['email'];
$mensaje = $_POST['form']['mensaje'];

$body = "<table>
    <tr>
        <td>Nombres: </td>
        <td>$nombre</td>
    </tr>
    <tr>
        <td>Teléfono: </td>
        <td>$telefono</td>
    </tr>
    <tr>
        <td>Email: </td>
        <td>$email</td>
    </tr>
    <tr>
        <td>Mensaje: </td>
        <td>$mensaje</td>
    </tr>
</table>";

$transport = (new Swift_SmtpTransport('condominioloscondores.com', 25))
    ->setUsername('noreply@condominioloscondores.com')
    ->setPassword('Zvuo51^2')
;

$mailer = new Swift_Mailer($transport);

$message = (new Swift_Message('Wonderful Subject'))
    ->setSubject('[Condores]')
    ->setFrom(['noreply@condominioloscondores.com' => 'Condores'])
    ->setReplyTo($email)
    ->setTo(['ventas@grupoconcreta.com']) // Aqui colocar el correo o los correos separados por comas a donde van a llegar los emails
    ->setBody($body, 'text/html')
;

$result = $mailer->send($message);

if ($result)
{
    $file = fopen("contacto.csv", "a+");
    fwrite($file, "$nombre,$telefono,$email,$mensaje" . PHP_EOL);
    fclose($file);
    header('Location: http://condominioloscondores.com/enviado.html');
}
else
{
    header('Location: http://condominioloscondores.com/error.html');
}
